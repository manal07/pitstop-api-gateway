# Utilisez une image de base officielle Node.js
FROM node:16-alpine

# Définir le répertoire de travail dans le conteneur
WORKDIR /api

# Copier package.json et package-lock.json dans le répertoire de travail
COPY package*.json ./

# Installer les dépendances de l'application
RUN npm install

# Copier le reste des fichiers de l'application dans le répertoire de travail
COPY . .

# Exposer le port sur lequel l'application écoute
EXPOSE 8080

# Définir la commande pour démarrer l'application
CMD ["node", "server.js"]
